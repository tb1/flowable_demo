package com.example.flowable.thymeleaf.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

/**
 * @author Foolish
 * @description
 * @date: 2018/9/21 16:16
 */

@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(){
        return "/demo/index";
    }

    @RequestMapping("/path/{path}")
    public String path(@PathVariable String path){
        return "/demo/" + path;
    }

    @RequestMapping("/one")
    public String one(){
        return "content1";
    }

    @RequestMapping("/two")
    public String two(){
        return "content2";
    }
}
